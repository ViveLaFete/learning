package assembler

import translators.COMP
import translators.DEST
import translators.JUMP
import translators.TranslatorService

class CInstruction {

    val translator = TranslatorService()
    val ZERO_VALUE_3 = "000"

    /**
     * @line: e.g. dest=comp;jump
     * Either the dest or jump fields may be empty
     * If dest is empty the "=" is ommitted
     * If jump is empty the ";" is ommitted
     */
    fun processCInstruction(line: String): String {

        val unusedBits = "111"
        val hasDest: Boolean = line.contains("=")
        val hasJump: Boolean = line.contains(";")

        val dest: String = if (hasDest) {
            val destVal: String = line.substring(0, line.indexOf('='))
            getDestBinCode(destVal)
        } else {
            ZERO_VALUE_3
        }

        val jump: String = if (hasJump) {
            val jumpValue: String = line.substring(line.indexOf(';') + 1)
            getJumpBinCode(jumpValue)
        } else {
            ZERO_VALUE_3
        }

        val compStartIndex: Int =
                if (hasDest) line.indexOf('=') + 1
                else 0

        val compEndIndex: Int =
                if (hasJump) line.indexOf(';')
                else line.length

        val comp = getCompBinCode(line.substring(compStartIndex, compEndIndex))
        return unusedBits + comp + dest + jump
    }

    private fun getCompBinCode(value: String): String {
        return translator.doTranslation(COMP, value)
    }

    private fun getDestBinCode(value: String): String {
        return translator.doTranslation(DEST, value)
    }

    private fun getJumpBinCode(value: String): String {
        return translator.doTranslation(JUMP, value)
    }
}