package assembler

class LabelVariables {

    private val labelVariables: MutableMap<String, Int> = hashMapOf()

    fun initialize(assemblyCode: List<String>): Map<String, Int> {
        var numberOfLabelVariables = 0
        for (i in 0 until assemblyCode.size) {
            val trimmedLine = assemblyCode[i].trim()
            if (trimmedLine.startsWith("(")) {
                val noParathenises = trimmedLine.replace("(", "").replace(")", "")
                labelVariables["@$noParathenises"] = i - numberOfLabelVariables
                numberOfLabelVariables++ //as the (...) is not itself an instruction need to ignore any that appear when setting mem address
            }
        }
        return labelVariables
    }


    fun exposeVariables(): Map<String, Int> = labelVariables

}