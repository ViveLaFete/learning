package runner

import IO.preformAssembly
import IO.writeListToFile

fun main(args: Array<String>) {
    val path = "C:\\Users\\esteele\\Documents\\stuff\\assembler\\learning\\nand2tetris\\projects\\06\\max\\MaxL.asm"
    val assembled = preformAssembly(path)
    println (path)
    writeListToFile(assembled, path.substring(path.indexOf('.') + 1) + ".hack")
}