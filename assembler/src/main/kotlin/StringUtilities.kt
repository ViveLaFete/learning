fun isStringNumber(line: String): Boolean {
    for (c: Char in line) {
        if (!Character.isDigit(c)) {
            return false
        }
    }
    return true
}

fun padTo16(value: String) : String {
    return value.padStart(16, '0')
}