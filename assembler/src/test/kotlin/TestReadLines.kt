import assembler.AInstruction
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class TestReadLines {

    @Test
    fun testReadNumber() {
        val parser = AInstruction()
        val res = parser.processAInstruction("@7")
        println(res)
        assertTrue(res.length == 16)
    }

}