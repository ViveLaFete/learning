import IO.preformAssembly
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TestParsingFiles {

    @Test
    fun testReadFile() {
        val assembled: List<String> = preformAssembly( System.getProperty("user.dir") + "/src/test/kotlin/resources/simpleProgram.asm")
        val correctResults: List<String> = listOf(
                "0000 0000 0001 0000",
                "1110 1111 1100 1000",
                "0000 0000 0001 0001",
                "1110 1010 1000 1000",
                "0000 0000 0001 0000",
                "1111 1100 0001 0000",
                "0000 0000 0110 0100",
                "1110 0100 1101 0000",
                "0000 0000 0001 0010",
                "1110 0011 0000 0001",
                "0000 0000 0001 0000",
                "1111 1100 0001 0000",
                "0000 0000 0001 0001",
                "1111 0000 1000 1000",
                "0000 0000 0001 0000",
                "1111 1101 1100 1000",
                "0000 0000 0000 0100",
                "1110 1010 1000 0111",
                "0000 0000 0001 0010",
                "1110 1010 1000 0111"
        )
        Assertions.assertEquals(correctResults.size, assembled.size)
        for (i in 0 until correctResults.size) {
            Assertions.assertEquals(correctResults[i], assembled[i])
        }
    }
}