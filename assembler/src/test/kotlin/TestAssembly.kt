import assembler.AInstruction
import assembler.Assembler
import assembler.CInstruction
import assembler.LabelVariables
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import translators.TranslatorService

class TestAssembly {

    @Test
    fun testReadNumber() {
        val translator = TranslatorService()
        val instruction = "jump"
        val bits = "JLT"

        val res = translator.doTranslation(instruction, bits)
        Assertions.assertTrue(res == "100")
    }

    @Test
    fun testAInstruction4() {
        val aInstruction = AInstruction()
        val res: String = aInstruction.processAInstruction("@4")
        Assertions.assertTrue(res == "0000000000000100")
    }

    @Test
    fun testAInstruction23() {
        val aInstruction = AInstruction()
        val res: String = aInstruction.processAInstruction("@23")
        Assertions.assertTrue(res == "0000000000010111")
    }

    @Test
    fun testAInstructionVariable() {
        val aInstruction = AInstruction()
        val res: String = aInstruction.processAInstruction("@i")
        Assertions.assertTrue(res == "0000000000010000")
    }

    @Test
    fun testAInstructionManyVariables() {
        //variables loaded into incremental memory address
        val aInstruction = AInstruction()
        val i: String = aInstruction.processAInstruction("@i")
        Assertions.assertTrue(i == "0000000000010000")

        val j: String = aInstruction.processAInstruction("@j")
        Assertions.assertTrue(j == "0000000000010001")

        val k: String = aInstruction.processAInstruction("@k")
        Assertions.assertTrue(k == "0000000000010010")

        val l: String = aInstruction.processAInstruction("@l")
        Assertions.assertTrue(l == "0000000000010011")
    }

    @Test
    fun testAssemblyInstruction() {
        val assembler = Assembler(AInstruction(), CInstruction(), LabelVariables())
        val line = "D=M"
        val lineAssembly = assembler.assembleLine(line)
        val expectedAns = "1111 1100 0001 0000"
        Assertions.assertTrue(lineAssembly.length == 16 + 3)
        Assertions.assertEquals(expectedAns, lineAssembly)
    }

    @Test
    fun testAssemblyInstructionDEqualsDMinusA() {
        val assembler = Assembler(AInstruction(), CInstruction(), LabelVariables())
        val line = "D=D-A"
        val lineAssembly = assembler.assembleLine(line)
        val expectedAns = "1110 0100 1101 0000"
        Assertions.assertTrue(lineAssembly.length == 16 + 3)
        Assertions.assertEquals(expectedAns, lineAssembly)
    }

    @Test
    fun testAssemblyInstructionMEqualsDPlusM() {
        val assembler = Assembler(AInstruction(), CInstruction(), LabelVariables())
        val line = "M=D+M"
        val lineAssembly = assembler.assembleLine(line)
        val expectedAns = "1111 0000 1000 1000"
        Assertions.assertTrue(lineAssembly.length == 16 + 3)
        Assertions.assertEquals(expectedAns, lineAssembly)
    }

    @Test
    fun testAssemblyInstructionWithComment() {
        val assembler = Assembler(AInstruction(), CInstruction(), LabelVariables())
        val line = "M=D+M //fdgdfgd"
        val lineAssembly = assembler.assembleLine(line)
        val expectedAns = "1111 0000 1000 1000"
        Assertions.assertTrue(lineAssembly.length == 16 + 3)
        Assertions.assertEquals(expectedAns, lineAssembly)
    }

    @Test
    fun testAssemblyInstructionDJGT() {
        val assembler = Assembler(AInstruction(), CInstruction(), LabelVariables())
        val line = "D;JGT"
        val lineAssembly = assembler.assembleLine(line)
        val expectedAns = "1110 0011 0000 0001"
        Assertions.assertTrue(lineAssembly.length == 16 + 3)
        Assertions.assertEquals(expectedAns, lineAssembly)
    }

    @Test
    fun testAssemblyInstructionInfiniteLoop() {
        val assembler = Assembler(AInstruction(), CInstruction(), LabelVariables())
        val line = "0;JMP"
        val lineAssembly = assembler.assembleLine(line)
        val expectedAns = "1110 1010 1000 0111"
        Assertions.assertTrue(lineAssembly.length == 16 + 3)
        Assertions.assertEquals(expectedAns, lineAssembly)
    }

    @Test
    fun testAssemblyInstructionDestCompAndJump() {
        val assembler = Assembler(AInstruction(), CInstruction(), LabelVariables())
        val line = "D=M;JGT"
        val lineAssembly = assembler.assembleLine(line)
        val expectedAns = "1111 1100 0001 0001"
        Assertions.assertTrue(lineAssembly.length == 16 + 3)
        Assertions.assertEquals(expectedAns, lineAssembly)
    }

}