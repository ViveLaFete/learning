--make a procedure that filters even fibonachi numbers

isEven :: Integral a => a -> Bool
isEven a =
  if (a `mod` 2 == 0) then True
    else False

genFib l r limit storeSize store =
  if (storeSize >= limit) then store
    else genFib r (l+r) limit (storeSize + 1) (l : store)

fibList = genFib 1 1 100 0 []

noOdds = filter isEven fibList
