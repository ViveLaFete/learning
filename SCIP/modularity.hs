

estimatePi trials =
    (sqrt (6 / (monteCarlo trials cesaroTest)))

cesaroTest = (gcd rand rand) == 1

{--pi estimated from the fraction of times gcd of two random numbers has
gcd of 1/

monteCarlo trials experiment =
    let iter trialsRemaining trialsPassed
        | trialsRemaining == 0 = trialsPassed / trials --base case
        | experiment == True = iter (trialsRemaining - 1) (trialsPassed + 1) --experiment a success
        | otherwise = iter (trialsRemaining - 1) (trialsPassed)
    in iter trials 0--}
