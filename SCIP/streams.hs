-- iterative style
sumPrimes a b =
  let iterate count accumulation = if (count > b) then accumulation
                                     else if (isPrime count) then iterate (count + 1) (count + accumulation)
                                       else iterate (count + 1) accumulation
  in iterate a 0

isPrime x =
  let testPrimalityIteratively divisor
    | (divisor * 2) > x = True
    | x `mod` divisor == 0 = False
    | otherwise = testPrimalityIteratively x (divisor + 1)
  in testPrimalityIteratively 2



--if no values in list div prime then is prime
--iterate- if new num be be divided by prime in list

findPrimes p =
  let testPrimalityIteratively primeList divisor lim
    | divisor > lim = primeList
    | x `mod` divisor == 0 = testPrimalityIteratively primeList (divisor + 1) lim
    | otherwise = testPrimalityIteratively primeList ++ [divisor] (divisor + 1) lim
