sumPrimes a b =
  let iterate count accumulation = if (count > b) then accumulation
                                    else if (isPrime count) then iterate (count + 1) (count + accumulation)
                                      else iterate (count + 1) accumulation
  in iterate a 0

isPrime x = testPrimalityIteratively x 2

testPrimalityIteratively x divisor
  | (divisor * 2) > x = True
  | x `mod` divisor == 0 = False
  | otherwise = testPrimalityIteratively x (divisor + 1)


getListPrimes start stop = fuckYouLet start stop []

fuckYouLet index stop primeList
  | index > stop = primeList
  | checkPastPrimes primeList index = fuckYouLet (index + 1) stop (primeList ++ [index])
  | otherwise = fuckYouLet (index + 1) stop primeList

checkPastPrimes [] toCheck = True
checkPastPrimes (x:xs) toCheck = if (toCheck `mod` x == 0) then False
                                  else checkPastPrimes xs toCheck
