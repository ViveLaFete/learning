--define last pair
aList = [23,72,129,34]

--filter list based on parity of the first element

sameParity lst =
  let parity = head(lst) `mod` 2
      fltr _ [] = []
      fltr p (x:xs)
        | p (x `mod` 2) = x : fltr p xs
        | otherwise = fltr p xs
  in fltr (== parity) lst

--apply procedure for every element in the list
myMap :: (t -> t1) -> [t] -> [t1]
myMap _ [] = []
myMap func (x:xs) = func x : myMap func xs
-- : means append

{-make an implementation of forEach difference with map is that it doesn't
return a list but rather simply applies each operation in turn

this is a bit weird in haskell as IO must be in main and compartmentalised -}

reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]

reverseList2 = go []
  where
    go acc [] = acc
    go acc (x:xs) = go (x:acc) xs

a = 1
b = 2
ab = [a, b]
a_b = ['a', 'b']
