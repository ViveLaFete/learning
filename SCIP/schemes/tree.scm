(define (count-leaves x)
  (cond ((null? x) 0)
     ((not (pair? x)) 1)
     (else (+ (count-leaves (car x))
              (count-leaves (cdr x))))))

(define wave2 (beside wave (flip-vert wave)))
(define wave4 (below wave2 wave2))
