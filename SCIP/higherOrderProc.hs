inc n = n + 1

--a function for doing abstract sum
-- @term = function, a is lowerBound, b is upperBound
ourSum f a next b =
  if (a > b) then 0 --if lower bound > upper bound
    else (f a) + ourSum term (next a) next b
    --do procedure then add to rest of series

sumIter runningTot term a next b =
  if(a > b) then runningTot
    else sumIter ((term a) + runningTot) term (next a) next b
    --calc new total then advance enumerator

myProduct runningTot term a next b =
  if(a > b) then runningTot
    else if (runningTot == 0)
      then myProduct ((term a) * 1) term (next a) next b
      else
        myProduct ((term a) * runningTot) term (next a) next b

myProdAlt term a next b =
  --doesn't solve zero problem
  let iter a res = if (a > b) then res
                    else iter (next a) ((term a) * res)
  in iter a 1


cube x = (x * x * x)

sumCubes a b = ourSum cube a inc b
sumIterCubes a b = sumIter 0 cube a inc b
prodIterCubes a b = myProdAlt cube a inc b

sumIntegers a b =
  let idTerm x = x
  in ourSum idTerm a inc b

prodInts a b =
  let idTerm x = x
  in myProduct 0 idTerm a inc b
--this is fact just in reverse

piSum a b =
  let piTerm x = (1.0 / (x * (x+2)))
      piNext y = (y + 4)
  in ourSum piTerm a piNext b

{-accumulate combiner nullValue term a next b =
  if (a > b) then nullValue
    else
      (term a) combiner (accumulate
      combiner nullValue term (next a) next b)

identical n = n

sumAcc term a next b = accumulate (+) 0 term a next b
prodAcc term a next b = accumulate (*) 1 term a next b
Come back to-}

--let <binding> in <expression>
--integral f a b dx =
  --(ourSum f (a + (dx / 2) (b + dx))) * dx

{-simpsons rule:
simpsonIntegral f a b n =
  (h / 3) * (ourSum simpsonTerm 0 inc n)
  where h = (a - b) / n
  --todo-}

--lambdas
piSum2 a b = 8 * ourSum (\x -> (1.0 / (x * (x+2)))) a (\x -> x + 4) b

integral' f a b dx = ourSum f (a + dx/2) (\x -> x + dx) b * dx

{-finding roots with half interval method
search f negPoint posPoint =
  let midpoint average negPoint posPoint =
    if isCloseEnough negPoint posPoint then midpoint
      else let testValue f midpoint
        | testValue > 0 = search f negPoint midpoint
        | testValue < 0 = search f midpoint posPoint
        | otherwise = midpoint

isCloseEnough x y = abs(x-y) < 0.001

halfIntervalMethod f a b
  | aValue < 0 and bValue > 0 = search f a b
  | aValue > 0 and bValue < 0 = search f b a
  | otherwise = error-}
--newton

deriv g = (\x -> (g (x + dx) - g x) / dx)
--deriv is a procedure that takes a procedure as an argument and returns
-- a procedure as a value
dx = 0.0000001
newtonTransform g = (\x -> g x / deriv g x - x)
newtonMethod g guess = fixedPoint (newtonTransform g) guess
--todo
