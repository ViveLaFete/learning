
--max2OfThree x y z =

min3 :: Ord a => a -> a -> a -> a
min3 x y z = min (min x y) (z)

removeMin :: Ord a => a -> a -> a -> (a, a)
removeMin x y z
  | x == minVal = (y,z)
  | y == minVal = (x,z)
  | z == minVal = (x,y)
  where minVal = min3 x y z

square :: Num a => a -> a
square n1 = n1 * n1

sumSquares :: (Int, Int) -> Int
sumSquares (x,y) = square (fst(x,y)) + square (snd(x,y))

allTogether x y z = sumSquares (removeMin x y z)
--aarghhh takes 3 nums and returns sum of squares largest 2

--iterative factorial
factorial :: (Num a, Ord a) => a -> a
factorial n = factIter 1 1 n

factIter :: (Num a, Ord a) => a -> a -> a -> a
factIter aProduct aCounter maxCount =
  if (aCounter > maxCount) then aProduct
    else factIter (aCounter * aProduct) (aCounter + 1) (maxCount)

--fib iter
--fib :: (Num a) => a -> a
fib n = fibIter 1 0 n

fibIter a b count =
  if (count == 0) then b
    else fibIter (a+b) a (count -1)


--count change problem
countChange amount = cc amount 5

cc amount kindsOfCoins
  | (amount == 0) = 1
  | (amount < 0) = 0
  | (kindsOfCoins == 0) = 0
  | otherwise = cc amount (kindsOfCoins-1) +
              cc (amount - (firstDenomination kindsOfCoins))
              kindsOfCoins

firstDenomination kindsOfCoins
  | kindsOfCoins == 1 = 1
  | kindsOfCoins == 2 = 5
  | kindsOfCoins == 3 = 10
  | kindsOfCoins == 4 = 25
  | kindsOfCoins == 5 = 50

--1.11 sicp recursive
f n
 | n < 3 = n
 | otherwise = f (n-1) + 2 * f (n-2) + 3 * f (n-3)

--iterative
f2 n
  | n < 3 = n
  | otherwise = fIter 2 1 0 n

{-return a + 2b + 3c
counts down from number to 3
-}
fIter a b c n
  | n < 3 = a
  | otherwise = fIter (a + (2*b) + (3*c)) a b (n-1)

pascal row column
  | column == 1 = 1
  | row == column = 1
  | otherwise = pascal (row-1) (column-1) +
      pascal (row-1) column

--exponentiation linear recursive O(n) time and space
--b^n = b . b ^ n-1
--b^0 = 1
expt b n
  | n == 0 = 1
  | otherwise = b * (expt b (n-1))

expt1 b n = exptIter b n 1

exptIter b counter product2 =
  if (counter == 0) then product2
    else exptIter b (counter - 1) (product2 * b)

{-fast for powers of 2
b^n = (b^n/2)^2 if even, b^n = b.b^n-1 if odd
this (i got into a mess with types here is logarithmic )
-}
fastExp :: Integral a => a -> a -> a
fastExp n b
  | n == 0 = 1
  | n `mod` 2 == 0 = square (fastExp b (n `div` 2))
  | otherwise = b * (fastExp b (n-1))

forLoop sumVal count limit =
  if count == limit then sumVal
  else forLoop (sumVal + (square (count)))
                      (count + 1) limit

--simple primality testing
smallestDivisor n = findDivisor n 2

findDivisor n testDiv
  | square testDiv > n = n --base case too big
  | n `mod` testDiv == 0 = testDiv --is a factor
  | otherwise = findDivisor n (nextNum testDiv)

isPrime n =
  if n == smallestDivisor n then True --if loop through all and still
    --no potentials then must be prime
    else False

nextNum n = if n == 2 then 3
  else n+2

{-fermat test: is n is a prime number and a is any postive Integer
less than n, then a raised to the nth power is congrutent to a
modulo n-}
