
BITSUM = "bitSum"
CARRY = "carry"

def halfAdd(op1, op2):
    asum = op1 ^ op2 
    carry = op1 and op2 
    return {
        BITSUM : asum,
        CARRY : carry
    }


def fullAdd(op1, op2, op3):
    halfAddAB = halfAdd(op1, op2)
    secondSum = halfAdd(halfAddAB[BITSUM], op3)
    carry = halfAddAB[CARRY] or secondSum[CARRY]
    return (secondSum[BITSUM], carry)

fullAddition = fullAdd(True, True, False)
print (fullAddition)