myFun x y = x * y

listComp = [x*2 | x <- [1..10]]

allEvens = [x | x <- [1..1000], x `mod` 2 == 0]