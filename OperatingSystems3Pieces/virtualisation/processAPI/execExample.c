#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <fcntl.h> 
#include <sys/wait.h> 

//e.g. > wc <file1> > <file2.txt> counts the words and prints to a file  
int main(int argc, char *argv[]) {
    int rc = fork();

    if (rc < 0) {
        fprintf(stderr, "fork failed\n");
        exit(1);
    } else if (rc == 0) {
        //child redirect standard output to a file 
        close(STDOUT_FILENO);
        open("./progOut.output", O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);

        //now exit "wc"
        char *myargs[3];
        myargs[0] = strdup("wc");
        myargs[1] = strdup("execExample.c"); //argument: file to count 
        myargs[2] = NULL; //marks end of array C WUT 
        execvp(myargs[0], myargs); //runs word count 
    } else {
        //parent process goes down this path and waits until child has completed 
        int rc_wait = wait(NULL);
    }
    return 0;
}

int func() {
    for (int i = 0; i < 100; i++) {
        printf("making a system call");
    }
}