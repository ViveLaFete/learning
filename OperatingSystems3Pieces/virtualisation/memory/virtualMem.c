#include <stdio.h> 
#include <stdlib.h> 

void mallocExample() {
    //allocating memory
    double *d = (double *) malloc(sizeof(double));
    //freeing memory -> not doing so causes a memory leak
    free(d);

    char *src = "hello";
    printf("src is %p\n", &src);
    printf("src value is %s\n", src);

    //buffer overflows not allocating enough memory
    char *dst = (char *) malloc(strlen(src)); //too small
    //this might write 1 byte past the end of the allocated space
    //and potentially overrite an important variable
    strcpy(dst, src);
}

int main(int argc, char *argv[]) {
    printf("location of code : %p\n", (void *) main);
    //create a void pointer- just a pointer with no explicit type 
    printf("location of heap : %p\n", (void *) malloc(1));

    int x = 3;
    printf("location of stack : %p\n", (void *) &x);

    mallocExample();
    return x;
}
